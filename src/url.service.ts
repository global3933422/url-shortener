import { HttpException, HttpStatus, Injectable, Inject } from '@nestjs/common';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { generate } from 'shortid';
import { Cache } from 'cache-manager';

@Injectable()
export class UrlService {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  async createShortUrl(longUrl: string): Promise<string> {
    const shortUrl = generate();
    await this.cacheManager.set(shortUrl, longUrl);
    return shortUrl;
  }

  async getLongUrl(shortUrl: string): Promise<string> {
    const url = await this.cacheManager.get(shortUrl);
    if (!url) {
      throw new HttpException('Short URL not found', HttpStatus.NOT_FOUND);
    }
    return <string>url;
  }
}
