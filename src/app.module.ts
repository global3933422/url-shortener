import { Module } from '@nestjs/common';
import { UrlService } from './url.service';
import { UrlController } from './url.controller';
import { CacheModule } from '@nestjs/common';
import 'dotenv/config';

@Module({
  imports: [
    CacheModule.register({
      isGlobal: true,
      ttl: parseInt(process.env.URL_EXPIRE),
    }),
  ],
  providers: [UrlService],
  controllers: [UrlController],
})
export class AppModule {}
