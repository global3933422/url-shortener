import { Controller, Post, Body, Get, Param, Redirect } from '@nestjs/common';
import { UrlService } from './url.service';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { LongUrlDto } from './dto/long-url.dto';
import { ShortUrlDto } from './dto/short-url.dto';
import { UrlResponse } from './dto/responses/redirect.dto';
import { ShortTokenResponse } from './dto/responses/get-token.dto';

@Controller('url')
@ApiTags('shortener')
export class UrlController {
  constructor(private readonly urlService: UrlService) {}

  @Post('shorten')
  @ApiOperation({
    summary: 'send logn url with body then get token',
  })
  @ApiResponse({
    status: 201,
    description: 'get token',
    type: ShortTokenResponse,
  })
  async shortenUrl(
    @Body() longUrlDto: LongUrlDto,
  ): Promise<ShortTokenResponse> {
    const shortUrl = await this.urlService.createShortUrl(longUrlDto.lognURL);
    return { token: shortUrl };
  }

  @Get(':shortURL')
  @ApiOperation({
    summary: 'get short url and then response original url then redirect',
  })
  @ApiResponse({
    status: 200,
    description: 'redirect to another page',
    type: UrlResponse,
  })
  @Redirect()
  async redirectToLongUrl(
    @Param() shortUrlDto: ShortUrlDto,
  ): Promise<UrlResponse> {
    const longUrl = await this.urlService.getLongUrl(shortUrlDto.shortURL);
    return { url: longUrl };
  }
}
