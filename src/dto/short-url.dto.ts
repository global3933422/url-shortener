import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class ShortUrlDto {
  @ApiProperty()
  @IsString()
  shortURL: string;
}
