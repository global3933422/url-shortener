import { ApiProperty } from '@nestjs/swagger';

export class ShortTokenResponse {
  @ApiProperty()
  token: string;
}
