import { ApiProperty } from '@nestjs/swagger';
import { IsUrl } from 'class-validator';

export class UrlResponse {
  @ApiProperty()
  @IsUrl()
  url: string;
}
