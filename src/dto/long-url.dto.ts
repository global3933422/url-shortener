import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUrl } from 'class-validator';

export class LongUrlDto {
  @ApiProperty()
  @IsUrl()
  @IsNotEmpty()
  lognURL: string;
}
